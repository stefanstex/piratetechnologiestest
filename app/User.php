<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get all user roles
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(){
        return $this->belongsToMany('App\Role');
    }

    /**
     * Check if user has some role
     * @param $role
     * @return bool
     */
    public function hasRole($role){
        $roles = $this->roles->lists('name');

        return in_array($role, $roles->toArray());
    }

    /**
     * Get user user posts
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts() {
        return $this->hasMany('App\Post');
    }

    /**
     * Check if this is first posting, based on email,
     * and maybe we want to return true if there is a post with status: 'pending' or 'spam',
     * because it is suspicious and moderator needs to approve next post
     * @param $email
     * @return bool
     */
    public function checkIfFirstPosting($email) {
        return $this->posts()
            ->where('email', $email)
            ->where('status', '!=', 'pending')
            ->where('status', '!=', 'spam')
            ->count() == 0 ? true : false;
    }

    /**
     * Get all Job board moderators
     * @return mixed
     */
    public static function getJobBoardModerators() {
        return self::whereHas(
            'roles', function($q){
            $q->where('name', 'Job board moderator');
        })->get();
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Post;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PostsController extends Controller
{
    /**
     * Show all posts
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        // get all posts
        $posts = Post::all();

        return view('posts.index', compact('posts'));
    }
    /**
     * Create new job offer
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        return view('posts.create');
    }

    /**
     * Store new post in db
     * @param PostRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PostRequest $request) {
        // define status (pending/published/spam)
        $status = 'pending';
        // get post title
        $title = $request->input('title');
        // get post description
        $description = $request->input('description');
        // get post email
        $email = $request->input('email');

        // create post
        $post = Auth::user()->posts()->create([
            'title' => $title,
            'description' => $description,
            'email' => $email,
            'status' => $status
        ]);

        // If post is successfully created
        if($post){

            // if this is first job posting
            if(Auth::user()->checkIfFirstPosting($email)) {
                // send email to HR manager
                $data = [];
                $data['email'] = Auth::user()->email;
                $data['subject'] = 'Hello HR manager';
                $data['msg'] = 'Your submission is in moderation.';
                $data['template'] = 'hr_manager';

                // send email
                $this->sendEMail($data);

                // send email to Job board moderators
                $data = [];
                $data['subject'] = 'Hello Job board moderator';
                $data['post_title'] = $title;
                $data['post_description'] = $description;
                $data['post_id'] = $post->id;
                $data['template'] = 'job_board_moderator';

                // get all Job board moderators
                $jobBoardModerators = User::getJobBoardModerators();
                // loop through all users and send them email
                if($jobBoardModerators) {
                    foreach($jobBoardModerators as $user) {
                        $data['email'] = $user->email;
                        $data['user'] = $user->id;

                        // send email
                        $this->sendEMail($data);
                    }
                }

                $messageAddition = 'This is your first job posting for this email and it needs to be approved by Job board moderator.';
            } else {
                // publish post
                $post->status = 'published';
                $post->save();

                $messageAddition = 'This post will be published automatically, because you already have published posts with this email.';
            }

            $message = [
                'flash_message' => 'Post has been created. ' . $messageAddition,
                'flash_message_type' => 'success'
            ];
        } else {
            $message = [
                'flash_message' => 'Post has not been created.',
                'flash_message_type' => 'danger'
            ];
        }

        return redirect()->back()->with($message);
    }

    public function edit($id, Request $request) {
        // get post by id
        $post = Post::findOrFail($id);
        // get status from request
        $status = $request->input('status');
        // set post status
        $post->status = $status;

        // update post
        if($post->save()) {
            $message = [
                'flash_message' => 'You have successfully ' . $status . ' this post.',
                'flash_message_type' => 'success'
            ];
        } else {
            $message = [
                'flash_message' => 'You have not successfully ' . $status . ' this post.',
                'flash_message_type' => 'danger'
            ];
        }

        return redirect('/')->with($message);
    }

    /**
     * Send Email
     * @param $data
     */
    private function sendEMail($data) {
        Mail::send('emails.tmpl', $data, function($message) use ($data)
        {
            $message->from(env('MAIL_USERNAME'))
                ->to($data['email'], 'Test')
                ->subject($data['subject']);
        });
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotHrManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::guard($guard)->check() || !Auth::user()->hasRole('HR manager')) {
            return redirect('/')->with([
                'flash_message' => 'You need to be logged in as Hr manager.',
                'flash_message_type' => 'danger'
            ]);
        }

        return $next($request);
    }
}
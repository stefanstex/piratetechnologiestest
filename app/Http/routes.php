<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {

    // Authentication routes...
    Route::post('auth/login', 'Auth\AuthController@postLogin');
    Route::get('auth/logout', 'Auth\AuthController@logout');

    // Home page
    Route::get('/', 'HomeController@index');

    // HR manager routes
    Route::group(['middleware' => ['hrmanager']], function () {
        // create new job offer
        Route::get('posts/create', 'PostsController@create');
        // store job offer in db
        Route::post('posts', 'PostsController@store');
    });

    // Job board moderator routes
    Route::group(['middleware' => ['jobboardmoderator']], function () {
        // list od posts
        Route::get('posts', 'PostsController@index');
        // Approve (publish) or mark post as a spam
        Route::get('posts/{id}/edit', 'PostsController@edit');
    });


});

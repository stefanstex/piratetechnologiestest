@extends('layouts.master')

@section('title') Job Offers @endsection

@section('content')
    <div class="content">
        <h1>Job Offers</h1>

        @if($posts->count())
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>Title</td>
                        <td>Description</td>
                        <td>Email</td>
                        <td>Status</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td>{{ $post->title }}</td>
                            <td>{{ $post->description }}</td>
                            <td>{{ $post->email }}</td>
                            <td>{{ $post->status }}</td>
                            <td>
                                <a href="{{ url('posts/' . $post->id . '/edit?status=published') }}">Approve (publish)</a><br/>
                                <a href="{{ url('posts/' . $post->id . '/edit?status=spam') }}">Mark as a spam</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No job offers.</p>
        @endif
    </div>
@endsection
@extends('layouts.master')

@section('title') Post new job offer @endsection

@section('content')
<!-- Main content -->
<div class="content">
    <div class="row">
        <h1>Post new job offer</h1>
        <!-- Error List -->
        @include('errors.list')

        <!-- form start -->
        {!! Form::open(['url' => 'posts']) !!}

            <div class="form-group">
                <label>Title</label>
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                <label>Description</label>
                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                <label>Email</label>
                {!! Form::email('email', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('POST', ['class' => 'btn btn-primary']) !!}
            </div>

        {!! Form::close() !!}
    </div>
</div>
@endsection
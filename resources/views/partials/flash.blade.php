@if(session()->has('flash_message'))
    <section class="content-header">
        <div class="alert alert-{{ session('flash_message_type') }} alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p>{{ session('flash_message') }}</p>
        </div>
    </section>
@endif
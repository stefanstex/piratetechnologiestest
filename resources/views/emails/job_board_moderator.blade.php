<p>Post title:</p>
<p>{{ $post_title }}</p>
<br/>
<p>Post description:</p>
<p>{{ $post_description }}</p>
<br/>
<p>
    <a href="{{ env('APP_URL') }}/posts/{{ $post_id }}/edit?status=published&login_user={{ $user }}">Approve (publish)</a>
</p>
<p>
    <a href="{{ env('APP_URL') }}/posts/{{ $post_id }}/edit?status=spam&login_user={{ $user }}">Mark as a spam</a>
</p>
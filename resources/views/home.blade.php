@extends('layouts.master')

@section('title') Home @endsection

@section('content')

    <div class="content">

        @if(Auth::guest())

            {!! Form::open(['url' => 'auth/login']) !!}

                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password" id="password">
                </div>

                <div class="form-group">
                    <input type="checkbox" name="remember"> Remember Me
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>

            {!! Form::close() !!}

        @else
            <p>
                Welcome {{ Auth::user()->name }}, your role is: {{ implode(', ', Auth::user()->roles->lists('name')->toArray()) }}
            </p>

            @if(Auth::user()->hasRole('HR manager'))
                <p>
                    You can post a job offers
                </p>
            @endif

            @if(Auth::user()->hasRole('Job board moderator'))
                <p>
                    You can see all job offers and approve (publish) or mark it as a spam.
                </p>
            @endif

        @endif
    </div>

@endsection
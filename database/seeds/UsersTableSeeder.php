<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create HR manager user
        if(!User::where('email', 'hrmanager@test.com')->count()) {
            $user = User::create([
                'name' => 'John',
                'email' => 'hrmanager@test.com',
                'password' => bcrypt('hrmanager')
            ]);

            DB::table('role_user')->insert([
                'user_id' => $user->id,
                'role_id' => 1
            ]);
        }

        // Create Job board moderator user
        if(!User::where('email', 'jobboardmoderator@test.com')->count()) {
            $user = User::create([
                'name' => 'Alex',
                'email' => 'jobboardmoderator@test.com',
                'password' => bcrypt('jobboardmoderator')
            ]);

            DB::table('role_user')->insert([
                'user_id' => $user->id,
                'role_id' => 2
            ]);
        }
    }
}

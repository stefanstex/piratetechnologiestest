<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create HR manager role
        if(!Role::where('name', 'HR manager')->count()) {
            Role::create([
                'id' => 1,
                'name' => 'HR manager'
            ]);
        }

        // create Job board moderator
        if(!Role::where('name', 'Job board moderator')->count()) {
            Role::create([
                'id' => 2,
                'name' => 'Job board moderator'
            ]);
        }
    }
}
